# SHACompareFiles
![SHACompareFiles](./screenshot.jpg) 

## Description
This program will compare the contents of two directories, and alert you to any files that are not identical.  It does so by generating a list of files in each directory, generating SHA-1 checksums for each file, and then comparing the checksums.  If the checksums match, then it is assumed the files are the same.    This program was primarily created to confirm that files copied from one hard drive to another are transfered without errors.  More information on SHA-1 can be found on [Wikipedia](http://en.wikipedia.org/wiki/SHA-1).

**License:** [GPL v.3](http://www.gnu.org/licenses/gpl-3.0.html)

## Instructions
Specify two folders to compare (Folder 1 and Folder 2).  Press "Compare Contents of Folder 1 and Folder 2".  The program will do the rest.

## History
**Version 1.0.0:**
> - Initial Release
> - Released on 9 May 2012.
>
> Download: [Version 1.0.0 Setup](/uploads/940d9a8937513620ec62017c39ca1449/SHACompareFiles100Setup.zip) | [Version 1.0.0 Source](/uploads/1c4b851dfeda7bbcf0d16185b523a8d0/SHACompareFiles100Source.zip)
