﻿namespace SHACompareFiles
{
    partial class FormCompareFiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCompareFiles));
            this.labelFolder1 = new System.Windows.Forms.Label();
            this.labelFolder2 = new System.Windows.Forms.Label();
            this.textBoxFolder1 = new System.Windows.Forms.TextBox();
            this.textBoxFolder2 = new System.Windows.Forms.TextBox();
            this.buttonSelectFolder1 = new System.Windows.Forms.Button();
            this.buttonSelectFolder2 = new System.Windows.Forms.Button();
            this.listViewFiles = new System.Windows.Forms.ListView();
            this.columnFileName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnFolder1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnFolder2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnChecksum1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnChecksum2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.statusStripResults = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelResults = new System.Windows.Forms.ToolStripStatusLabel();
            this.buttonCompare = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.progressChecksum = new System.Windows.Forms.ProgressBar();
            this.statusStripResults.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelFolder1
            // 
            this.labelFolder1.AutoSize = true;
            this.labelFolder1.Location = new System.Drawing.Point(12, 30);
            this.labelFolder1.Name = "labelFolder1";
            this.labelFolder1.Size = new System.Drawing.Size(48, 13);
            this.labelFolder1.TabIndex = 0;
            this.labelFolder1.Text = "Folder 1:";
            // 
            // labelFolder2
            // 
            this.labelFolder2.AutoSize = true;
            this.labelFolder2.Location = new System.Drawing.Point(12, 52);
            this.labelFolder2.Name = "labelFolder2";
            this.labelFolder2.Size = new System.Drawing.Size(48, 13);
            this.labelFolder2.TabIndex = 1;
            this.labelFolder2.Text = "Folder 2:";
            // 
            // textBoxFolder1
            // 
            this.textBoxFolder1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFolder1.Location = new System.Drawing.Point(66, 27);
            this.textBoxFolder1.Name = "textBoxFolder1";
            this.textBoxFolder1.Size = new System.Drawing.Size(478, 20);
            this.textBoxFolder1.TabIndex = 2;
            // 
            // textBoxFolder2
            // 
            this.textBoxFolder2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFolder2.Location = new System.Drawing.Point(66, 49);
            this.textBoxFolder2.Name = "textBoxFolder2";
            this.textBoxFolder2.Size = new System.Drawing.Size(478, 20);
            this.textBoxFolder2.TabIndex = 3;
            // 
            // buttonSelectFolder1
            // 
            this.buttonSelectFolder1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSelectFolder1.Location = new System.Drawing.Point(550, 25);
            this.buttonSelectFolder1.Name = "buttonSelectFolder1";
            this.buttonSelectFolder1.Size = new System.Drawing.Size(77, 23);
            this.buttonSelectFolder1.TabIndex = 4;
            this.buttonSelectFolder1.Text = "Select Folder";
            this.buttonSelectFolder1.UseVisualStyleBackColor = true;
            this.buttonSelectFolder1.Click += new System.EventHandler(this.buttonSelectFolder1_Click);
            // 
            // buttonSelectFolder2
            // 
            this.buttonSelectFolder2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSelectFolder2.Location = new System.Drawing.Point(550, 47);
            this.buttonSelectFolder2.Name = "buttonSelectFolder2";
            this.buttonSelectFolder2.Size = new System.Drawing.Size(77, 23);
            this.buttonSelectFolder2.TabIndex = 5;
            this.buttonSelectFolder2.Text = "Select Folder";
            this.buttonSelectFolder2.UseVisualStyleBackColor = true;
            this.buttonSelectFolder2.Click += new System.EventHandler(this.buttonSelectFolder2_Click);
            // 
            // listViewFiles
            // 
            this.listViewFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewFiles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnFileName,
            this.columnFolder1,
            this.columnFolder2,
            this.columnChecksum1,
            this.columnChecksum2});
            this.listViewFiles.Location = new System.Drawing.Point(12, 104);
            this.listViewFiles.Name = "listViewFiles";
            this.listViewFiles.Size = new System.Drawing.Size(615, 259);
            this.listViewFiles.TabIndex = 6;
            this.listViewFiles.UseCompatibleStateImageBehavior = false;
            this.listViewFiles.View = System.Windows.Forms.View.Details;
            // 
            // columnFileName
            // 
            this.columnFileName.Text = "File Name";
            this.columnFileName.Width = 150;
            // 
            // columnFolder1
            // 
            this.columnFolder1.Text = "Folder 1";
            this.columnFolder1.Width = 150;
            // 
            // columnFolder2
            // 
            this.columnFolder2.Text = "Folder 2";
            this.columnFolder2.Width = 150;
            // 
            // columnChecksum1
            // 
            this.columnChecksum1.Text = "Checksum 1";
            this.columnChecksum1.Width = 71;
            // 
            // columnChecksum2
            // 
            this.columnChecksum2.Text = "Checksum 2";
            this.columnChecksum2.Width = 71;
            // 
            // statusStripResults
            // 
            this.statusStripResults.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelResults});
            this.statusStripResults.Location = new System.Drawing.Point(0, 395);
            this.statusStripResults.Name = "statusStripResults";
            this.statusStripResults.Size = new System.Drawing.Size(639, 22);
            this.statusStripResults.TabIndex = 7;
            this.statusStripResults.Text = "statusStrip1";
            // 
            // toolStripStatusLabelResults
            // 
            this.toolStripStatusLabelResults.Name = "toolStripStatusLabelResults";
            this.toolStripStatusLabelResults.Size = new System.Drawing.Size(171, 17);
            this.toolStripStatusLabelResults.Text = "Select Two Folders to Compare";
            // 
            // buttonCompare
            // 
            this.buttonCompare.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonCompare.Location = new System.Drawing.Point(211, 75);
            this.buttonCompare.Name = "buttonCompare";
            this.buttonCompare.Size = new System.Drawing.Size(217, 23);
            this.buttonCompare.TabIndex = 8;
            this.buttonCompare.Text = "Compare Contents of Folder 1 and Folder 2";
            this.buttonCompare.UseVisualStyleBackColor = true;
            this.buttonCompare.Click += new System.EventHandler(this.buttonCompare_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(639, 24);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // progressChecksum
            // 
            this.progressChecksum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressChecksum.Location = new System.Drawing.Point(12, 369);
            this.progressChecksum.Name = "progressChecksum";
            this.progressChecksum.Size = new System.Drawing.Size(615, 23);
            this.progressChecksum.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressChecksum.TabIndex = 10;
            // 
            // FormCompareFiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(639, 417);
            this.Controls.Add(this.progressChecksum);
            this.Controls.Add(this.buttonCompare);
            this.Controls.Add(this.statusStripResults);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.listViewFiles);
            this.Controls.Add(this.buttonSelectFolder2);
            this.Controls.Add(this.buttonSelectFolder1);
            this.Controls.Add(this.textBoxFolder2);
            this.Controls.Add(this.textBoxFolder1);
            this.Controls.Add(this.labelFolder2);
            this.Controls.Add(this.labelFolder1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormCompareFiles";
            this.Text = "Compare Files";
            this.statusStripResults.ResumeLayout(false);
            this.statusStripResults.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelFolder1;
        private System.Windows.Forms.Label labelFolder2;
        private System.Windows.Forms.TextBox textBoxFolder1;
        private System.Windows.Forms.TextBox textBoxFolder2;
        private System.Windows.Forms.Button buttonSelectFolder1;
        private System.Windows.Forms.Button buttonSelectFolder2;
        private System.Windows.Forms.ListView listViewFiles;
        private System.Windows.Forms.StatusStrip statusStripResults;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelResults;
        private System.Windows.Forms.Button buttonCompare;
        private System.Windows.Forms.ColumnHeader columnFolder1;
        private System.Windows.Forms.ColumnHeader columnFolder2;
        private System.Windows.Forms.ColumnHeader columnChecksum1;
        private System.Windows.Forms.ColumnHeader columnChecksum2;
        private System.Windows.Forms.ColumnHeader columnFileName;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ProgressBar progressChecksum;
    }
}

