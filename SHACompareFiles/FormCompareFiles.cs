﻿//  SHA Compare Files
//  Copyright 2012 Eric Cavaliere
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or (at
//  your option) any later version.
//
//  This program is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA  02110-1301, USA.
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;

namespace SHACompareFiles
{
    public partial class FormCompareFiles : Form
    {
        public FormCompareFiles()
        {
            InitializeComponent();
        }

        private void buttonSelectFolder1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialogFolder1 = new FolderBrowserDialog();
            if (dialogFolder1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBoxFolder1.Text = dialogFolder1.SelectedPath;
            }
            dialogFolder1.Dispose();
        }

        private void buttonSelectFolder2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialogFolder2 = new FolderBrowserDialog();
            if (dialogFolder2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBoxFolder2.Text = dialogFolder2.SelectedPath;
            }
            dialogFolder2.Dispose();
        }

        private void getFolderContents(string strFolderPath, ref List<string> listFiles)
        {
            DirectoryInfo dirInfoFolder = new DirectoryInfo(strFolderPath);

            foreach (FileInfo oneFile in dirInfoFolder.GetFiles())
            {
                listFiles.Add(oneFile.FullName);
                Application.DoEvents();
            }

            foreach (DirectoryInfo oneFolder in dirInfoFolder.GetDirectories())
            {
                getFolderContents(oneFolder.FullName, ref listFiles);
            }
        }

        private void buttonCompare_Click(object sender, EventArgs e)
        {
            if (textBoxFolder1.Text == "")
            {
                MessageBox.Show("Please Select Folder 1.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (textBoxFolder2.Text == "")
            {
                MessageBox.Show("Please Select Folder 2.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (textBoxFolder1.Text == textBoxFolder2.Text) 
            {
                MessageBox.Show("Please Select Different Locations for Folder 1 and Folder 2.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DirectoryInfo dirInfoFolder1 = new DirectoryInfo(textBoxFolder1.Text);
            DirectoryInfo dirInfoFolder2 = new DirectoryInfo(textBoxFolder2.Text);

            if (!dirInfoFolder1.Exists)
            {
                MessageBox.Show("Folder 1 Does Not Exist.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!dirInfoFolder2.Exists)
            {
                MessageBox.Show("Folder 2 Does Not Exist.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            buttonCompare.Enabled = false;
            textBoxFolder1.Enabled = false;
            textBoxFolder2.Enabled = false;
            buttonSelectFolder1.Enabled = false;
            buttonSelectFolder2.Enabled = false;

            List<string> listFolder1 = new List<string>(), listFolder2 = new List<string>();
            toolStripStatusLabelResults.Text = "Generating List of Files in Folder 1...";
            Application.DoEvents();
            getFolderContents(textBoxFolder1.Text, ref listFolder1);

            toolStripStatusLabelResults.Text = "Generating List of Files in Folder 2...";
            Application.DoEvents();
            getFolderContents(textBoxFolder2.Text, ref listFolder2);

            toolStripStatusLabelResults.Text = "Loading File List...";
            listViewFiles.Visible = false;
            Application.DoEvents();

            foreach (string s in listFolder1)
            {
                ListViewItem newFile = new ListViewItem();
                newFile.Text = s.Substring(textBoxFolder1.Text.Length);
                newFile.SubItems.Add(s);
                newFile.SubItems.Add("");
                listViewFiles.Items.Add(newFile);
                Application.DoEvents();
            }
            
            foreach (string s in listFolder2)
            {
                bool boolFoundFile = false;
                foreach (ListViewItem oneListItem in listViewFiles.Items)
                {
                    if (oneListItem.Text == s.Substring(textBoxFolder2.Text.Length))
                    {
                        oneListItem.SubItems[2].Text = s;
                        boolFoundFile = true;
                        break;
                    }
                    Application.DoEvents();
                }
                if (boolFoundFile == false)
                {
                    ListViewItem newFile = new ListViewItem();
                    newFile.Text = s.Substring(textBoxFolder2.Text.Length);
                    newFile.SubItems.Add("");
                    newFile.SubItems.Add(s);
                    listViewFiles.Items.Add(newFile);
                }
                Application.DoEvents();
            }
            progressChecksum.Maximum = listViewFiles.Items.Count;

            listViewFiles.Visible = true;
            toolStripStatusLabelResults.Text = "Generating File Checksums...";
            Application.DoEvents();

            SHA1 sha1Checksum = new SHA1Managed();
            int intBadFiles = 0;

            foreach (ListViewItem listOneFile in listViewFiles.Items)
            {
                if (listOneFile.SubItems[1].Text != "")
                {
                    try
                    {
                        FileInfo oneFile = new FileInfo(listOneFile.SubItems[1].Text);
                        FileStream oneFileStream = oneFile.Open(FileMode.Open);
                        oneFileStream.Position = 0;
                        string strSHA1Hash = BitConverter.ToString(sha1Checksum.ComputeHash(oneFileStream));
                        oneFileStream.Close();
                        listOneFile.SubItems.Add(strSHA1Hash);
                    }
                    catch
                    {
                        listOneFile.SubItems.Add("<Error>");
                    }
                    Application.DoEvents();
                }
                else
                {
                    listOneFile.SubItems.Add("");
                    Application.DoEvents();
                }

                if (listOneFile.SubItems[2].Text != "")
                {
                    try
                    {
                        FileInfo oneFile = new FileInfo(listOneFile.SubItems[2].Text);
                        FileStream oneFileStream = oneFile.Open(FileMode.Open);
                        oneFileStream.Position = 0;
                        string strSHA1Hash = BitConverter.ToString(sha1Checksum.ComputeHash(oneFileStream));
                        oneFileStream.Close();
                        listOneFile.SubItems.Add(strSHA1Hash);
                    }
                    catch
                    {
                        listOneFile.SubItems.Add("[Error]");
                    }

                    Application.DoEvents();
                }
                else
                {
                    listOneFile.SubItems.Add("");
                    Application.DoEvents();
                }

                if (listOneFile.SubItems[3].Text != listOneFile.SubItems[4].Text)
                {
                    listOneFile.Font = new Font(listViewFiles.Font, FontStyle.Bold);
                    intBadFiles++;
                    Application.DoEvents();
                }

                progressChecksum.Value++;
                Application.DoEvents();
            }

            if (intBadFiles == 0) {
                MessageBox.Show("All Files Matched.", "Finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
                toolStripStatusLabelResults.Text = "All Files Matched.";
            } else {
                MessageBox.Show(intBadFiles.ToString() + " File(s) Did Not Match.", "Finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
                toolStripStatusLabelResults.Text = intBadFiles.ToString() + " File(s) Did Not Match.";
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAbout windowAbout = new FormAbout();
            windowAbout.ShowDialog();
        }
    }
}
